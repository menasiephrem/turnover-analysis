FROM rappdw/docker-java-python:openjdk1.8.0_171-python3.6.6
COPY . .
RUN pip install -r requirements.txt
EXPOSE 5000
ENTRYPOINT ["./gunicorn.sh"]
