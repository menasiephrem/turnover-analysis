

import os
import ProcessPDF_InMNew
import ProcessPDF_Stanbic
import ProcessPDF_DTB
import ProcessPDF_SCB
import ProcessPDF_InMNew
import ProcessPDF_COOP
import ProcessPDF_CASA
import ProcessPDF_ABSA
from flask_cors import CORS

from flask import Flask, request, send_file, jsonify


app = Flask(__name__)
CORS(app, resources=r'/reports', headers='Content-Type')
UPLOAD_DIRECTORY = 'c:\\tmp\\'


def process(bank, inputfile, password, outputFile):
    if bank == 'STANBIC':
        ProcessPDF_Stanbic.process(inputfile, password, outputFile)
    elif bank == 'I&M':
        ProcessPDF_InMNew.process(inputfile, password, outputFile)
    elif bank == 'CASA':
        ProcessPDF_CASA.process(inputfile, password, outputFile)
    elif bank == 'SCB':
        ProcessPDF_SCB.process(inputfile, password, outputFile)
    elif bank == 'COOP':
        ProcessPDF_COOP.process(inputfile, password, outputFile)
    elif bank == 'ABSA':
        ProcessPDF_ABSA.process(inputfile, password, outputFile)
    elif bank == 'DTB':
        return ProcessPDF_DTB.process(inputfile, password, outputFile)


@app.route('/reports', methods=['POST'])
def get_reports():
    print('----------')
    bank = request.values['bank']
    password = request.values['password']
    print(password)
    # """Upload a file."""
    outputFile = "c:\\tmp\\stanbic.xlsx"
    uploaded_file = request.files['file']
    print(uploaded_file.filename)
    print(uploaded_file)
    if uploaded_file.filename != '':
        uploaded_file.save(UPLOAD_DIRECTORY+uploaded_file.filename)
        process(bank, UPLOAD_DIRECTORY +
                uploaded_file.filename, password, outputFile)
    return send_file(
        outputFile,
        mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        as_attachment=True)


# app.run(debug=False, port=port)
