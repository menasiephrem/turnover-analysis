
import numpy as np
import tabula
import pandas as pd
import datetime

import math

from future.backports.total_ordering import total_ordering
from openpyxl import Workbook
from openpyxl.styles import colors
from openpyxl.styles import Font, Color
from openpyxl.utils import FORMULAE
from openpyxl.styles import numbers

from numpy.core.defchararray import strip
import PyPDF2




def  transaction_type(row):
    # print(row)
    if 'DR' in row['BALANCE']:
        return -1
    else:
        return 1
#
# epoch = datetime.datetime.utcfromtimestamp(0)
#
# def unix_time_millis(dt):
#     return (dt - epoch).total_seconds() * 1000.0
def readtoDF(inputFile,password):
    df = tabula.read_pdf(inputFile, pages='all', stream=True, guess=True, multiple_tables=True, password=password,
                         lattice=True)

    accountInfo = tabula.read_pdf(inputFile, pages=1, stream=True, guess=False, multiple_tables=True, password=password,
                         lattice=False,area=(102, 280, 160,534))

    print(accountInfo)



    tempFiratPageDF=accountInfo[0]
    tempFiratPageDF=tempFiratPageDF.iloc[:, [0, 1]]
    tempFiratPageDF.loc[-1] = tempFiratPageDF.columns.values
    tempFiratPageDF.sort_index(inplace=True)
    tempFiratPageDF.reset_index(drop=True, inplace=True)

    tempFiratPageDF.columns = [ 'key', 'value']
    print('----------------======')
    print(tempFiratPageDF)

    accountDetails = dict(zip(tempFiratPageDF.key, tempFiratPageDF.value))
    print(accountDetails)

    df = pd.concat(df, ignore_index=True)

    return df,accountDetails

def formatPDF(df):
    pd.set_option('display.max_columns', None)

    print(df)
    df = df.iloc[:, [0, 2, 3, 4]]
    df.columns.values[0] = "Date"
    df.columns.values[1] = "Debit"
    df.columns.values[2] = "Credit"
    df.columns.values[3] = "Balance"
    df = df.dropna()
    df = df[~df.Date.str.contains('Trn Dt')]
    df = df[~df.Date.str.contains('USD')]
    df = df[~df.Date.str.contains('Total')]
    return df



def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

def processBalanceDF(BalanceDF):

    BalanceDF['MONTHYR'] = pd.to_datetime(BalanceDF['Date'], format='%d%b%y').dt.to_period('M')
    BalanceDF['Balance'] = BalanceDF['Balance'].str.replace(',', '')
    BalanceDF['Balance'] = pd.to_numeric(BalanceDF['Balance'])
    BalanceDF = BalanceDF.groupby('Date').tail(1)
    TempBalanceDF = (BalanceDF.set_index('Date').reindex(pd.date_range(
        datetime.datetime.strptime(BalanceDF['Date'].head(1).iloc[0].strip(), '%d%b%y') + datetime.timedelta(days=1),
        datetime.datetime.strptime(BalanceDF['Date'].tail(1).iloc[0].strip(), '%d%b%y') - datetime.timedelta(days=1),
        freq='D')).rename_axis(['Date']).reset_index())

    TempBalanceDF['MONTHYR'] = pd.to_datetime(TempBalanceDF['Date'], format='%Y-%m-%d').dt.to_period('M')

    TempBalanceDF['ORDER'] = TempBalanceDF.apply(
        lambda row: unix_time_millis(pd.to_datetime(row['Date'], format='%Y-%m-%d')),
        axis=1)
    BalanceDF['ORDER'] = BalanceDF.apply(lambda row: unix_time_millis(pd.to_datetime(row['Date'], format='%d%b%y')),
                                         axis=1)

    TempBalanceDF['Date'] = TempBalanceDF['Date'].dt.strftime("%d-%m-%Y")
    BalanceDF['Date'] = pd.to_datetime(BalanceDF['Date'].str.lower(), format='%d%b%y').dt.strftime('%d-%m-%Y')

    BalanceDFIndex = BalanceDF.set_index('Date').index
    TempBalanceDFIndex = TempBalanceDF.set_index('Date').index
    mask = ~TempBalanceDFIndex.isin(BalanceDFIndex)
    TempBalanceDFIndex = TempBalanceDF.loc[mask]

    BalanceDF = pd.concat([BalanceDF, TempBalanceDFIndex], ignore_index=True)
    BalanceDF.sort_values(by='ORDER', inplace=True)

    BalanceDF.drop(['ORDER'], axis=1, inplace=True)
    BalanceDF['Balance'].fillna(method='pad', inplace=True)

    return BalanceDF

def processDebitCreditDF(DebitCreditDF):
    DebitCreditDF['Date'] = pd.to_datetime(DebitCreditDF['Date'].str.lower(), format='%d%b%y').dt.strftime('%d-%m-%Y')
    DebitCreditDF['Debit'] = pd.to_numeric(DebitCreditDF['Debit'].str.replace(',', ''))
    DebitCreditDF['Credit'] = pd.to_numeric(DebitCreditDF['Credit'].str.replace(',', ''))
    return DebitCreditDF

def finalProcessDF(BalanceDF,DebitCreditDF):
    DetailDF = pd.merge(BalanceDF, DebitCreditDF, how='left', on='Date')
    AggDF = DetailDF.groupby(['MONTHYR'], as_index=False).agg(
        {'Debit': 'sum', 'Credit': 'sum', 'Balance': ['min', 'max', 'mean']})
    AggDF.columns = ['Period', 'Min_Balance', 'Max_Balance', 'Avg_Balance', 'Debit_Turnover', 'Credit_Turnover']
    return DetailDF,AggDF

#
# def dftoExcel(DetailDF,AggDF):
#     wb = Workbook()
#     summary = wb.create_sheet("Summary")
#     summary.title = "Summary"
#     trxns = wb.create_sheet("Trxns")
#     summary.title = "Trxns"
#     with pd.ExcelWriter('e:\\output2.xlsx') as writer:
#         DetailDF.to_excel(writer, sheet_name='summary')
#         AggDF.to_excel(writer, sheet_name='trxns')


def toExcel(DetailDF,AggDF,AcctInfo,outputFile):
    wb = Workbook()
    ws1 = wb.create_sheet("Sheet1")
    ws1.title = "Aggregated"
    ws2 = wb.create_sheet("Sheet2")
    ws2.title = "Detailed"

    std = wb.get_sheet_by_name('Sheet')

    wb.remove_sheet(std)

    ws1['A1'].font = Font(bold=True)
    ws1['A1'] = 'A/C Name'
    # ws1['B1'] = AcctInfo['Account Title:']
    ws1.merge_cells('B1:F1')

    ws1['A2'].font = Font(bold=True)
    ws1['A2'] = 'A/C No'
    ws1['B2'] = AcctInfo['Account Number :']

    ws1['C2'].font = Font(bold=True)
    ws1['C2'] = 'Bank'
    ws1['D2'] = 'SCB Kenya'

    ws1['E2'].font = Font(bold=True)
    ws1['E2'] = 'Cur'
    ws1['F2'] = AcctInfo['Currency :']

    ws1['A3'].font = Font(bold=True)
    ws1['A3'] = 'Period'
    ws1['B3'].font = Font(bold=True)
    ws1['B3'] = 'Min Balance'
    ws1['C3'].font = Font(bold=True)
    ws1['C3'] = 'Max Balance'
    ws1['D3'].font = Font(bold=True)
    ws1['D3'] = 'Average Balance'
    ws1['E3'].font = Font(bold=True)
    ws1['E3'] = 'Debit Turnover'
    ws1['F3'].font = Font(bold=True)
    ws1['F3'] = 'Credit Turnover'
    ws1['G3'].font = Font(bold=True)
    ws1['G3'] = 'Overdraft Utilization'

    ws1['J1'].font = Font(bold=True)
    ws1['J1'] = 'Overdraft Limit'
    ws1['J2'] = str(0)

    start=4
    j=start
    for i in AggDF.itertuples():
        ws1['A'+str(j)] = str(i[1])
        ws1['B' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['B'+str(j)] = i[2]
        ws1['C' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['C'+str(j)] = i[3]
        ws1['D' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['D'+str(j)] = i[4]
        ws1['E' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['E'+str(j)] = i[5]
        ws1['F' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['F'+str(j)] = i[6]
        ws1['G' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['G' + str(j)] = '=ABS(IF(B'+str(j)+'<0,B'+str(j)+'/$J$2,0))*100'
        j+=1
    end=j-1

    ws1['A' + str(j)].font = Font(bold=True)
    ws1['A' + str(j)] = 'Minimum'
    ws1['B' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['B' + str(j)] = "=Min(B"+str(start)+":B"+str(end)+")"

    j+=1
    ws1['A' + str(j)].font = Font(bold=True)
    ws1['A' + str(j)] = 'Maximum'
    ws1['C' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['C' + str(j)] = "=Max(C" + str(start) + ":C" + str(end) + ")"

    j += 1
    ws1['A' + str(j)].font = Font(bold=True)
    ws1['A' + str(j)] = 'Average'
    ws1['D' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['D' + str(j)] = "=Average(D" + str(start) + ":D" + str(end) + ")"
    ws1['E' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['E' + str(j)] = "=Average(E" + str(start) + ":E" + str(end) + ")"
    ws1['F' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['F' + str(j)] = "=Average(F" + str(start) + ":F" + str(end) + ")"

    j += 1
    ws1['A' + str(j)].font = Font(bold=True)
    ws1['A' + str(j)] = 'Total'
    ws1['E' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['E' + str(j)] = "=Sum(E" + str(start) + ":E" + str(end) + ")"
    ws1['F' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['F' + str(j)] = "=Sum(F" + str(start) + ":F" + str(end) + ")"







###############################

    ws2['A1'].font = Font(bold=True)
    ws2['A1'] = 'Date'
    ws2['B1'].font = Font(bold=True)
    ws2['B1'] = 'Balance'
    ws2['C1'].font = Font(bold=True)
    ws2['C1'] = 'Debit'
    ws2['D1'].font = Font(bold=True)
    ws2['D1'] = 'Credit'
    ws2['E1'].font = Font(bold=True)
    ws2['E1'] = 'Period'



    start = 2
    j = start
    for i in DetailDF.itertuples():
        ws2['A' + str(j)] = str(i[1])
        ws2['B' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws2['B' + str(j)] = i[2]
        ws2['C' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws2['C' + str(j)] = i[4]
        ws2['D' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws2['D' + str(j)] = i[5]
        ws2['E' + str(j)] = str(i[3])

        j += 1
    end = j - 1

    wb.save(outputFile)

if __name__ == '__main__':
    print("Process Started")
    process('C:\\Users\\mnomikhan\Downloads\Statement USD - SCBK.pdf','0619',"e:\\scb.xlsx")
    print('Process Ends')


def process(inputFile,password,outputFile):
    print("Process Started")
    df, accountDetails = readtoDF(
        inputFile=inputFile,
        password=password)
    df = formatPDF(df)
    BalanceDF = processBalanceDF(df[['Date', 'Balance']])
    DebitCreditDF = processDebitCreditDF(df[['Date', 'Debit', 'Credit']])
    DetailDF, AggDF = finalProcessDF(BalanceDF, DebitCreditDF)
    toExcel(DetailDF, AggDF, accountDetails, outputFile)
    print('Process Ends')
