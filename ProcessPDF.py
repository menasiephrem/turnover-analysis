
import numpy as np
import tabula
import pandas as pd
import datetime
import math
from numpy.core.defchararray import strip


def tran_type(x):
    print(x)

def k(t):
    print(t['||'][1:10])
    t['||'][1:2].isdigit()

def l(t):
    print(t)
    t['||'][1:2].isdigit()


def  transaction_type(row):
    # print(row)
    if 'Dr' in row['BALANCE']:
        return -1
    else:
        return 1

epoch = datetime.datetime.utcfromtimestamp(0)

def unix_time_millis(dt):
    # print(dt)
    # return (dtstrip(), format='%d-%m-%y',errors='ignore')
    # print(dt - epoch)
    return (dt - epoch).total_seconds() * 1000.0


def read_imbank_pdf():
    mpesa_file = 'C:\\Users\\mnomikhan\\Downloads\\FW__Bank_Statements_-_Sample\\I&M Bank Statement.pdf'
    output_file = 'E:\\bankstatement.csv'
    dfs = tabula.read_pdf(mpesa_file, pages='all', stream=True, guess=True, multiple_tables=False)
    #                       # password=password)
    # dfs=pd.read_csv('d:\my_data.csv')
    dfs=dfs[0]
    dfs['FULL'] = dfs[dfs.columns[0:]].apply(
        lambda x: ','.join(x.dropna().astype(str)),
        axis=1
    )
    dfs=dfs[['FULL']]
    new_df=dfs.dropna()

    new_df = new_df[new_df["FULL"].str.startswith('|')]
    new_df = new_df[~new_df["FULL"].str.startswith('||')]
    new_df = new_df[~new_df.FULL.str.contains('TRAN| VALUE')]
    new_df = new_df[~new_df.FULL.str.contains('DATE|  DATE')]
    df=new_df.FULL.str.split(pat='|', expand=True)
    df=df.drop(df.columns[0],axis=1)
    df = df.drop(df.columns[7], axis=1)

    dict={df.columns[0]:'TRAN DATE',df.columns[1]:'VALUE DATE',df.columns[2]:'CHQ/REF.NO',df.columns[3]:'NARRATION',df.columns[4]:'WITHDRAWAL AMOUNT',df.columns[5]:'DESPOSIT AMOUNT',df.columns[6]:'BALANCE'}
    df.rename(columns=dict,
              inplace=True)

    df=df.drop(['NARRATION'],axis=1)
    df['TRAN DATE']=df['TRAN DATE'].str.strip()

    # print(df['TRAN DATE'].head(10))

    # print(df.dtypes)

    df=df.dropna()



    df['MONTHYR'] = pd.to_datetime(df['TRAN DATE'], format='%d-%m-%y').dt.to_period('M')

    df['MUL'] = df.apply(lambda row: transaction_type(row), axis=1)
    df['BALANCE'] = df['BALANCE'].str.replace('Dr','')
    df['BALANCE'] = df['BALANCE'].str.replace('Cr', '')
    df['BALANCE'] = df['BALANCE'].str.replace(',', '')
    df['WITHDRAWAL AMOUNT'] = pd.to_numeric(df['WITHDRAWAL AMOUNT'].str.replace(',',''))
    df['DESPOSIT AMOUNT'] = pd.to_numeric(df['DESPOSIT AMOUNT'].str.replace(',',''))
    df['BALANCE']=pd.to_numeric(df['BALANCE'])*pd.to_numeric(df['MUL'])
    df = df.drop(['MUL'], axis=1)


    df=df.groupby('TRAN DATE').tail(1)
    st=datetime.datetime.strptime(df['TRAN DATE'].head(1).iloc[0],'%d-%m-%y')
    et=datetime.datetime.strptime(df['TRAN DATE'].tail(1).iloc[0],'%d-%m-%y')
    date_df=(df.set_index('TRAN DATE').reindex(pd.date_range(datetime.datetime.strptime(df['TRAN DATE'].head(1).iloc[0].strip(),'%d-%m-%y')+datetime.timedelta(days=1), datetime.datetime.strptime(df['TRAN DATE'].tail(1).iloc[0].strip(),'%d-%m-%y')-datetime.timedelta(days=1), freq='D')).rename_axis(['TRAN DATE']).reset_index())



    date_df['TRAN DATE'] = pd.to_datetime(date_df['TRAN DATE'], format='%d-%m-%y', errors='ignore')

    cond = ~date_df['TRAN DATE'].isin(df['TRAN DATE'])
    date_df.drop(date_df[cond].index, inplace=True)
    date_df['MONTHYR'] = pd.to_datetime(date_df['TRAN DATE'], format='%d-%m-%y').dt.to_period('M')


    date_df['ORDER'] =date_df.apply(lambda row : unix_time_millis(row['TRAN DATE']), axis = 1)
    df['ORDER'] =df.apply(lambda row : unix_time_millis(pd.to_datetime(row['TRAN DATE'], format='%d-%m-%y')), axis = 1)

    date_df['TRAN DATE']= date_df['TRAN DATE'].dt.strftime('%d-%m-%y')


    df=pd.concat([df,date_df],ignore_index=True)
    df.sort_values(by='ORDER', inplace=True)
    df = df.groupby('ORDER').head(1)
    df.drop(['ORDER'],axis=1,inplace=True)
    df['BALANCE'].fillna(method='pad', inplace=True)
    df.to_csv(r'd:\im.csv', index=False)
    # df['WITHDRAWAL AMOUNT'].fillna(method='pad', inplace=True)
    # df['DESPOSIT AMOUNT'].fillna(method='pad', inplace=True)

    result=pd.DataFrame()
    result=df.groupby(['MONTHYR']).agg({'BALANCE':['min','max','mean'],'WITHDRAWAL AMOUNT':'sum','DESPOSIT AMOUNT':'sum'})
    result.columns=result.columns.droplevel()

    # result = df.groupby('MONTHYR').agg({'BALANCE': ['mean', 'min', 'max']})
    result.columns=['Minimum Balance','Maximum Balance','Average Balance','Debit  Turnover','Credit Turnover']
    result.index.names = ['Period']

    # =result.rename(columns={'MONTHYR':'1','mean':'2','max':'3','min':'4','sum':'5','sum':'6'})
    # df.to_csv(r'd:\my_data73.csv', index=False)
    print(result.columns)
    print(result)
    result.to_csv(r'd:\summary.csv', index=True)

if __name__ == '__main__':
    read_imbank_pdf()