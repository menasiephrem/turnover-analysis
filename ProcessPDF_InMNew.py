
import numpy as np
import tabula
import pandas as pd
import datetime

import math

from future.backports.total_ordering import total_ordering
from openpyxl import Workbook
from openpyxl.styles import colors
from openpyxl.styles import Font, Color
from openpyxl.utils import FORMULAE
from openpyxl.styles import numbers

from numpy.core.defchararray import strip
import PyPDF2




def  transaction_type(row):
    # print(row)
    if 'Dr' in row['Balance']:
        return -1
    else:
        return 1
#
# epoch = datetime.datetime.utcfromtimestamp(0)
#
# def unix_time_millis(dt):
#     return (dt - epoch).total_seconds() * 1000.0
def readtoDF(inputFile,password):
    file = open(inputFile, 'rb')
    readpdf = PyPDF2.PdfFileReader(file)
    totalpages = readpdf.numPages


    df =tabula.read_pdf(inputFile, pages='2-'+repr(totalpages), stream=True, guess=True, multiple_tables=True,pandas_options={'header': None})

    df1 = tabula.read_pdf(inputFile, pages=1, stream=True, guess=True, multiple_tables=True)

    accountInfo = tabula.read_pdf(inputFile, pages=1, stream=True, guess=False, multiple_tables=True,
                                  lattice=False, area=(155, 487,178, 654))
    df[0].to_csv('d:\\my_data23.csv', index=False)
    df[1].to_csv('d:\\my_data24.csv', index=False)
    df[2].to_csv('d:\\my_data25.csv', index=False)
    df[3].to_csv('d:\\my_data26.csv', index=False)


    for dfTemp in df:
        dfTemp['FULL'] = dfTemp[dfTemp.columns[0:]].apply(
            lambda x: ','.join(x.dropna().astype(str)),
            axis=1
        )
        # dfTemp['Date'] = dfTemp.iloc[:, 0].str.split(pat='|', expand=True)[1]
        # dfTemp['Debit'] = dfTemp.iloc[:, 1]
        # dfTemp['Credit'] = dfTemp.iloc[:, 2]
        # dfTemp['Balance'] = dfTemp.iloc[:, 3]
        # dfTemp = dfTemp[['Date', 'Debit', 'Credit', 'Balance']]
    dft = pd.DataFrame()

    for dfTemp in df:
        dft=dft.append(dfTemp[['FULL']])

    dft['Date'] = dft.iloc[:, 0].str.split(pat='|', expand=True)[1]
    dft['Debit'] = dft.iloc[:, 0].str.split(pat='|', expand=True)[5]
    dft['Credit'] = dft.iloc[:, 0].str.split(pat='|', expand=True)[6]
    dft['Balance'] = dft.iloc[:, 0].str.split(pat='|', expand=True)[7]
    dft.to_csv('d:\\my_data28.csv', index=False)

    df=dft

    df1 = pd.concat(df1, ignore_index=True)

    # df1[0].to_csv('d:\\my_data27.csv', index=False)

    print(df)
    print(df1)
    print(accountInfo[0])
    tempFiratPageDF = accountInfo[0]
    tempFiratPageDF = tempFiratPageDF.iloc[:, [0]]

    tempFiratPageDF.loc[-1] = tempFiratPageDF.columns.values
    tempFiratPageDF.sort_index(inplace=True)
    tempFiratPageDF.reset_index(drop=True, inplace=True)
    print('+++++++++')
    print(tempFiratPageDF.columns)
    tempFiratPageDF = tempFiratPageDF.iloc[:, 0].str.split(pat=':', expand=True)
    tempFiratPageDF.sort_index(inplace=True)
    tempFiratPageDF.reset_index(drop=True, inplace=True)
    tempFiratPageDF.columns = ['key', 'value']
    print(tempFiratPageDF)
    accountDetails = {}
    for i in tempFiratPageDF.itertuples():
        accountDetails[i[1]] = i[2]

    # accountDetails = dict(zip(tempFiratPageDF.key, tempFiratPageDF.value))
    print(accountDetails)
    #
    # df[0].to_csv('d:\\my_data19.csv', index=False)
    # df[1].to_csv('d:\\my_data21.csv', index=False)
    # df1.to_csv('d:\\my_data20.csv', index=False)

    # df = pd.concat(df, ignore_index=True)

    # df =  pd.concat([df1,df], ignore_index=True,axis=0)
    # df = df.replace('\r', ' ', regex=True)
    # df1 = df1.replace('\r', ' ', regex=True)


    # df = df.iloc[:, [0, 1, 2, 3]]
    # df['Date'] = df.iloc[:, 0].str.split(pat='|', expand=True)[1]
    # df['Debit'] = df.iloc[:, 1]
    # df['Credit'] = df.iloc[:, 2]
    # df['Balance'] = df.iloc[:, 3]
    # print('--------------------------')
    # print(df)
    # df1 = df1.iloc[:, [0, 2, 3, 4]]
    df1['Date'] = df1.iloc[:, 0].str.split(pat='|', expand=True)[1]
    df1['Debit'] = df1.iloc[:, 5]
    df1['Credit'] = df1.iloc[:, 6]
    df1['Balance'] = df1.iloc[:, 7]

    df=df[['Date','Debit','Credit','Balance']]
    df1 = df1[['Date', 'Debit', 'Credit', 'Balance']]


    # df1.to_csv('d:\\my_data20.csv', index=False)
    # exit(0)


    df = pd.concat([df1,df], ignore_index=True)
    # df.to_csv('d:\\my_data30.csv', index=False)
    # exit(0)
    #
    # new_header = df.iloc[1]  # grab the first row for the header
    # df = df[2:]  # take the data less the header row
    # df.columns = new_header

    return df,accountDetails

def formatPDF(df):
    pd.set_option('display.max_columns', None)

    print(df)
    # df = df.iloc[:, [0, 1, 2, 3]]
    # df.columns.values[0] = "Date"
    # df.columns.values[1] = "Debit"
    # df.columns.values[2] = "Credit"
    # df.columns.values[3] = "Balance"
    df = df[df['Date'].notna()]
    df = df[~df.Date.str.contains('Trn Dt')]
    df = df[~df.Date.str.contains('USD')]
    df = df[~df.Date.str.contains('Total')]
    df = df[~df.Date.str.contains('  TRAN')]
    df = df[~df.Date.str.contains('  DATE')]

    df['Debit'] = df['Debit'].str.replace('|', '')
    df['Credit'] = df['Credit'].str.replace('|', '')
    df['Balance'] = df['Balance'].str.replace('|', '')
    df = df[df['Date'].notna()]
    print(df)
    df['MUL'] = df.apply(lambda row: transaction_type(row), axis=1)
    print(pd)
    df['Balance'] = df['Balance'].str.replace('Dr', '')
    df['Balance'] = df['Balance'].str.replace('Cr', '')
    df['Balance'] = df['Balance'].str.replace(',', '')
    df['Balance'] = df['Balance'].str.replace(',', '')
    df['Date'] = df['Date'].str.replace(',', '')




    df['Balance'] = pd.to_numeric(df['Balance']) * pd.to_numeric(df['MUL'])
    df = df.drop(['MUL'], axis=1)


    return df



def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

def processBalanceDF(BalanceDF):
    format='%d-%m-%y'

    BalanceDF['Date']=BalanceDF['Date'].str.strip()
    BalanceDF['Date'].replace('', np.nan, inplace=True)

    BalanceDF = BalanceDF[BalanceDF['Date'].notna()]
    BalanceDF.to_csv('d:\\my_balance.csv', index=False)
    print(BalanceDF)
    BalanceDF['MONTHYR'] = pd.to_datetime(BalanceDF['Date'], format=format).dt.to_period('M')
    BalanceDF['Balance'] = BalanceDF['Balance'].replace(',', '')
    BalanceDF['Balance'] = pd.to_numeric(BalanceDF['Balance'])
    BalanceDF = BalanceDF.groupby('Date').tail(1)
    TempBalanceDF = (BalanceDF.set_index('Date').reindex(pd.date_range(
        datetime.datetime.strptime(BalanceDF['Date'].head(1).iloc[0].strip(), format) + datetime.timedelta(days=1),
        datetime.datetime.strptime(BalanceDF['Date'].tail(1).iloc[0].strip(), format) - datetime.timedelta(days=1),
        freq='D')).rename_axis(['Date']).reset_index())

    TempBalanceDF['MONTHYR'] = pd.to_datetime(TempBalanceDF['Date'], format=format).dt.to_period('M')

    TempBalanceDF['ORDER'] = TempBalanceDF.apply(
        lambda row: unix_time_millis(pd.to_datetime(row['Date'], format='%Y-%m-%d')),
        axis=1)
    BalanceDF['ORDER'] = BalanceDF.apply(lambda row: unix_time_millis(pd.to_datetime(row['Date'], format=format)),
                                         axis=1)

    TempBalanceDF['Date'] = TempBalanceDF['Date'].dt.strftime("%d-%m-%Y")
    BalanceDF['Date'] = pd.to_datetime(BalanceDF['Date'].str.lower(), format=format).dt.strftime('%d-%m-%Y')

    BalanceDFIndex = BalanceDF.set_index('Date').index
    TempBalanceDFIndex = TempBalanceDF.set_index('Date').index
    mask = ~TempBalanceDFIndex.isin(BalanceDFIndex)
    TempBalanceDFIndex = TempBalanceDF.loc[mask]

    BalanceDF = pd.concat([BalanceDF, TempBalanceDFIndex], ignore_index=True)
    BalanceDF.sort_values(by='ORDER', inplace=True)

    BalanceDF.drop(['ORDER'], axis=1, inplace=True)
    BalanceDF['Balance'].fillna(method='pad', inplace=True)

    return BalanceDF

def processDebitCreditDF(DebitCreditDF):
    DebitCreditDF['Date'] = DebitCreditDF['Date'].str.strip()
    DebitCreditDF['Date'].replace('', np.nan, inplace=True)

    DebitCreditDF = DebitCreditDF[DebitCreditDF['Date'].notna()]
    format = '%d-%m-%y'
    DebitCreditDF['Date'] = pd.to_datetime(DebitCreditDF['Date'].str.lower(), format=format).dt.strftime('%d-%m-%Y')
    DebitCreditDF['Debit'] = pd.to_numeric(DebitCreditDF['Debit'].str.replace(',', ''))
    DebitCreditDF['Credit'] = pd.to_numeric(DebitCreditDF['Credit'].str.replace(',', ''))
    return DebitCreditDF

def finalProcessDF(BalanceDF,DebitCreditDF):
    DetailDF = pd.merge(BalanceDF, DebitCreditDF, how='left', on='Date')
    AggDF = DetailDF.groupby(['MONTHYR'], as_index=False).agg(
        {'Debit': 'sum', 'Credit': 'sum', 'Balance': ['min', 'max', 'mean']})
    AggDF.columns = ['Period', 'Min_Balance', 'Max_Balance', 'Avg_Balance', 'Debit_Turnover', 'Credit_Turnover']
    return DetailDF,AggDF

#
# def dftoExcel(DetailDF,AggDF):
#     wb = Workbook()
#     summary = wb.create_sheet("Summary")
#     summary.title = "Summary"
#     trxns = wb.create_sheet("Trxns")
#     summary.title = "Trxns"
#     with pd.ExcelWriter('e:\\output2.xlsx') as writer:
#         DetailDF.to_excel(writer, sheet_name='summary')
#         AggDF.to_excel(writer, sheet_name='trxns')


def toExcel(DetailDF,AggDF,AcctInfo,outputFile):
    wb = Workbook()
    ws1 = wb.create_sheet("Sheet1")
    ws1.title = "Aggregated"
    ws2 = wb.create_sheet("Sheet2")
    ws2.title = "Detailed"

    std = wb.get_sheet_by_name('Sheet')

    wb.remove_sheet(std)

    ws1['A1'].font = Font(bold=True)
    ws1['A1'] = 'A/C Name'
    # ws1['B1'] = AcctInfo['Customer Name']
    ws1.merge_cells('B1:F1')

    ws1['A2'].font = Font(bold=True)
    ws1['A2'] = 'A/C No'
    ws1['B2'] = AcctInfo['ACCOUNT NUMBER ']

    ws1['C2'].font = Font(bold=True)
    ws1['C2'] = 'Bank'
    ws1['D2'] = 'I&M Kenya'

    ws1['E2'].font = Font(bold=True)
    ws1['E2'] = 'Cur'
    ws1['F2'] = AcctInfo['CURRENCY']

    ws1['A3'].font = Font(bold=True)
    ws1['A3'] = 'Period'
    ws1['B3'].font = Font(bold=True)
    ws1['B3'] = 'Min Balance'
    ws1['C3'].font = Font(bold=True)
    ws1['C3'] = 'Max Balance'
    ws1['D3'].font = Font(bold=True)
    ws1['D3'] = 'Average Balance'
    ws1['E3'].font = Font(bold=True)
    ws1['E3'] = 'Debit Turnover'
    ws1['F3'].font = Font(bold=True)
    ws1['F3'] = 'Credit Turnover'
    ws1['G3'].font = Font(bold=True)
    ws1['G3'] = 'Overdraft Utilization'

    ws1['J1'].font = Font(bold=True)
    ws1['J1'] = 'Overdraft Limit'
    ws1['J2'] = str(0)

    start=4
    j=start
    for i in AggDF.itertuples():
        ws1['A'+str(j)] = str(i[1])
        ws1['B' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['B'+str(j)] = i[2]
        ws1['C' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['C'+str(j)] = i[3]
        ws1['D' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['D'+str(j)] = i[4]
        ws1['E' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['E'+str(j)] = i[5]
        ws1['F' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['F'+str(j)] = i[6]
        ws1['G' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws1['G' + str(j)] = '=ABS(IF(B'+str(j)+'<0,B'+str(j)+'/$J$2,0))*100'
        j+=1
    end=j-1

    ws1['A' + str(j)].font = Font(bold=True)
    ws1['A' + str(j)] = 'Minimum'
    ws1['B' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['B' + str(j)] = "=Min(B"+str(start)+":B"+str(end)+")"

    j+=1
    ws1['A' + str(j)].font = Font(bold=True)
    ws1['A' + str(j)] = 'Maximum'
    ws1['C' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['C' + str(j)] = "=Max(C" + str(start) + ":C" + str(end) + ")"

    j += 1
    ws1['A' + str(j)].font = Font(bold=True)
    ws1['A' + str(j)] = 'Average'
    ws1['D' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['D' + str(j)] = "=Average(D" + str(start) + ":D" + str(end) + ")"
    ws1['E' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['E' + str(j)] = "=Average(E" + str(start) + ":E" + str(end) + ")"
    ws1['F' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['F' + str(j)] = "=Average(F" + str(start) + ":F" + str(end) + ")"

    j += 1
    ws1['A' + str(j)].font = Font(bold=True)
    ws1['A' + str(j)] = 'Total'
    ws1['E' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['E' + str(j)] = "=Sum(E" + str(start) + ":E" + str(end) + ")"
    ws1['F' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
    ws1['F' + str(j)] = "=Sum(F" + str(start) + ":F" + str(end) + ")"







###############################

    ws2['A1'].font = Font(bold=True)
    ws2['A1'] = 'Date'
    ws2['B1'].font = Font(bold=True)
    ws2['B1'] = 'Balance'
    ws2['C1'].font = Font(bold=True)
    ws2['C1'] = 'Debit'
    ws2['D1'].font = Font(bold=True)
    ws2['D1'] = 'Credit'
    ws2['E1'].font = Font(bold=True)
    ws2['E1'] = 'Period'



    start = 2
    j = start
    for i in DetailDF.itertuples():
        ws2['A' + str(j)] = str(i[1])
        ws2['B' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws2['B' + str(j)] = i[2]
        ws2['C' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws2['C' + str(j)] = i[4]
        ws2['D' + str(j)].number_format = numbers.FORMAT_NUMBER_COMMA_SEPARATED1
        ws2['D' + str(j)] = i[5]
        ws2['E' + str(j)] = str(i[3])

        j += 1
    end = j - 1

    wb.save(outputFile)

if __name__ == '__main__':
    print("Process Started")
    process('C:\\Users\\mnomikhan\\Downloads\\FW__Bank_Statements_-_Sample\\I&M Bank Statement.pdf', '0619', "e:\\InM.xlsx")
    print('Process Ends')

def process(inputFile,password,outputFile):
    print("Process Started")
    df, accountDetails = readtoDF(
        inputFile=inputFile,
        password=password)
    df = formatPDF(df)
    BalanceDF = processBalanceDF(df[['Date', 'Balance']])
    DebitCreditDF = processDebitCreditDF(df[['Date', 'Debit', 'Credit']])
    DetailDF, AggDF = finalProcessDF(BalanceDF, DebitCreditDF)
    toExcel(DetailDF, AggDF, accountDetails, outputFile)
    print('Process Ends')